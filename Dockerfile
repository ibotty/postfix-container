FROM registry.access.redhat.com/ubi8/ubi-init
LABEL MAINTAINER="Tobias Florek <tob@butter.sh>"

EXPOSE 25/tcp 587/tcp

COPY ./root/ /

RUN set -x \
 && dnf --setopt=tsflags=nodocs -y install postfix-pcre postfix-ldap \
 && rpm -q postfix \
 && dnf clean all \
 && systemctl enable postfix

# log only config messages
CMD ["/usr/sbin/init"]

VOLUME ["/etc/postfix", "/var/spool/postfix", "/var/lib/postfix"]
